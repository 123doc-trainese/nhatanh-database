# Lý thuyết

- [mysql](https://www.mysql.com/)
- [kiến thức nền tảng](https://trello.com/1/cards/62bd644ab33baf5b1faa410b/attachments/62c175605b16792a13cbc19a/download/SQL_Fundamentals.pdf)

# Kiến thức cần nắm được

- hiểu được database là gì
- sơ đồ ER & cách vẽ sơ đồ ER
- cách tạo `database` trên `mysql server`, cách tạo bảng, các kiểu dữ liệu
- truy vấn sql:
 - `select`, `update`, `delete`
 - `join` và các loại join 
 - ...
- tối ưu truy vấn

# Tài liệu tham khảo

- [giáo trình môn csdl](https://drive.google.com/drive/folders/1--RRSbqJU-N0twMaA9jsm99x9nN2h0Ky?usp=sharing)

# Task

 - [BaiTapER.docx](https://trello.com/1/cards/63084a5739b62a001f314f2a/attachments/630c8e4eb5157300764dabc0/download/BaiTapER.docx) 

- [BaiTapCSDL.docx](https://trello.com/1/cards/63084a5739b62a001f314f2a/attachments/630c8e4d885d060171f723f8/download/BaiTapCSDL.docx) 

- [BaiTapSQL.doc](https://trello.com/1/cards/63084a5739b62a001f314f2a/attachments/630c8e4f897bd1013f5e98a7/download/BaiTapSQL.doc) 
# Kiến thức đã nắm được
## Cơ sở dữ liệu 
    - là 1 tập hợp các dữ liệu có liên quan đến nhau, được lưu trữ trên máy tính, có nhiều người sử dụng và được tổ chức theo 1 mô hình
## Sơ đồ ER và cách vẽ sơ đồ
### Sơ đồ ER
    - mô hình thực thể - quan hệ là mô hình biểu diễn các quan hệ của thực thể với nhau
    - các thành phần chính: 
        - Thực thể : là các đối tượng, sự vật, đồ vật. được ký hiệu bằng hình chữ nhật
        - Thuộc tính : là các thuộc tính liên quan đến thực thể. vd con người có ngày sinh, tên, tuổi. được ký hiệu bằng hình bầu dục hoặc tròn
                       thuộc tính dùng để phân biệt các thực thể gọi là khoá. tương tự thuộc tính có thêm gạch chân dưới tên
    - mối quan hệ : thể hiện quan hệ giữa các thực thể. được quản lý bằng hình thoi
                    các kiểu quan hệ: 1 - n, 1 - 1, n - n
### Cách vẽ sơ đồ ER                
    - Hình chữ nhật: biểu diễn thực thể
    - Hình elip: biểu diễn thuộc tính, trong hình elip có ghi tên thuộc tính
    - Hình thoi: biểu diễn quan hệ

    - Các bước vẽ sơ đồ erd:

        - Thông qua việc liệt kê và lựa chọn thông tin dựa trên giấy tờ, hồ sơ
        - Xác định mối quan hệ giữa thực thể và thuộc tính của nó
        - Xác định mối quan hệ có thể có giữa các thực thể và mối kết hợp
        - Vẽ mô hình erd bằng các ký hiệu sau đó chuẩn hóa và thu gọn sơ đồ
### Cách tạo database, bảng và các kiểu dữ liệu

 - Cách tạo database
   ``` 
    CREATE DATABASE databasename;
   ```
  - Cách tạo bảng 
```
    CREATE TABLE table_name (
        column1 datatype,
        column2 datatype,
        ...
        PRIMARY KEY (column1),
        FOREIGN KEY (column2) REFERENCES table_name (column1)
    );
```
    - các kiểu dữ liệu 
```
    - kiểu số: tinyint, mediumint, bigint, int, float, double

    - ngày và giờ: date, datetime, time, year, timestamp

    - kiểu chữ: varchar, char, text, longtext, mediumtext, json

    - Danh sách: set, enum
    
    - Binary

    - Geometry
```
### Truy vấn sql
```
    select column1, column2 from table_name

    update table_name set column1 = `value` where column2 = `value`

    delete from table_name where column1 = `value`
```
    - các loại join
```
    1, join  
    select * from table_name1 tb1 join table_name2 tb2 on tb1.column1 = tb2.column1 
    
    2, left join
    select * from table_name1 tb1 left join table_name2 tb2 on tb1.column1 = tb2.column1

    3, right join
    select * from table_name1 tb1 right join table_name2 tb2 on tb1.column1 = tb2.column1

    4, full join
    select * from table_name1 tb1 full outer join table_name2 tb2 on tb1.column1 = tb2.column1

    5, self join 
    select * from table_name1 tb1 self join table_name1 tb2 on tb1.column1 = tb2.column2
```
